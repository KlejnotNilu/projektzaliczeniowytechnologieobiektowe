# Projektzaliczeniowytechnologieobiektowe

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

# Before You run

This app requires mongoDB running in the background along with local Node server

You can run mongoDB by following command(inside mongodb/bin directory):

`./mongod --dbpath="<path to folder inside which local database will be created>"`

You can run local Node server by running following npm script from `package.json`:

`npm run start-server`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

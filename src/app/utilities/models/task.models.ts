export interface TaskModel {
  completed: boolean,
  _id: string,
  description: string,
  owner: string,
  createdAt: Date,
  updatedAt: Date,
  __v: string
}

export type ColumnNames = 'description' | 'completed' | 'updatedAt' | 'createdAt' | undefined;

export type SortOrderTypes = 'asc' | 'desc';

export interface TaskPreferencesModel {
  completed: boolean | undefined,
  sort: {
    columnName: ColumnNames,
    order: SortOrderTypes
  },
  paginate: {
    limit: number,
    skip: number
  }
}

export interface TaskPreferenceChangesModel {
  completed?: boolean | undefined,
  sort?: {
    columnName?: ColumnNames,
    order?: SortOrderTypes
  },
  paginate?: {
    limit?: number,
    skip?: number
  }
}

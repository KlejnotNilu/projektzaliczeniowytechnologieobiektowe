export interface UserModel {
  _id: string,
  name: string,
  email: string,
  createdAt: Date,
  updatedAt: Date,
  __v: number
}

export interface UserLoginResponseModel {
  user: UserModel,
  token: string
}

export interface UserChanges {
  name?: string,
  password?: string
}

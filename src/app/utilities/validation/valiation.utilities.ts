import { FormControl, FormGroup, FormGroupDirective, NgForm } from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material";

export function getFirstErrorKeyOfFormControl(formControl: FormControl): string {
  return formControl.errors && Object.keys(formControl.errors).length ? Object.keys(formControl.errors)[0] : null;
}

export function validationErrorKeyToMessage(validationErrorKey: string): string {
  switch (validationErrorKey) {
    case 'required':
      return 'Sorry mate, this field is required.';
    case 'minlength':
      return 'You\'ll have to provide at least 6 characters';
    case 'maxlength':
      return 'Sorry, we\'re out of disk space. 50 characters is absolute maximum.';
    case 'email':
      return 'C\'mon, please enter valid email';
    case 'pattern':
      return `That's 99 tasks at once maximum`;
    default:
      return 'I have no idea what you did, but you\'ve broken it'
  }
}

export function passwordConfirmationValidator(group: FormGroup) {
  return group.get('password').value === group.get('confirm-password').value ? null : {confirmationFailed: true};
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid &&
      control.parent.dirty && control.parent.errors && Object.keys(control.parent.errors) &&
      Object.keys(control.parent.errors).includes('confirmationFailed'));

    return control.touched && (invalidCtrl || invalidParent);
  }
}

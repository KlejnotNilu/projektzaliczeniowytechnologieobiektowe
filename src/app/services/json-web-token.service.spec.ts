import { TestBed } from '@angular/core/testing';

import { JsonWebTokenService } from './json-web-token.service';

describe('JsonWebTokenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JsonWebTokenService = TestBed.get(JsonWebTokenService);
    expect(service).toBeTruthy();
  });
});

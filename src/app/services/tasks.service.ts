import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

import {
  TaskModel,
  TaskPreferenceChangesModel,
  TaskPreferencesModel
} from "../utilities/models/task.models";

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  taskPreferences: TaskPreferencesModel = {
    completed: undefined,
    sort: {
      columnName: undefined,
      order: "desc"
    },
    paginate: {
      limit: 10,
      skip: 0
    }
  };

  changePreferences(changes: TaskPreferenceChangesModel): void {
    Object.assign(this.taskPreferences, changes);
  }

  getPreferences(): TaskPreferencesModel {
    return this.taskPreferences;
  }

  private get getTaskPreferencesPathParams(): string {
    let pathParams = '';
    if (this.taskPreferences.completed !== undefined) {
      pathParams += `completed=${this.taskPreferences.completed}`;
    }
    if (this.taskPreferences.sort.columnName !== undefined) {
      if (pathParams !== '') {
        pathParams += '&';
      }
      pathParams += `sort=${this.taskPreferences.sort.columnName}:${this.taskPreferences.sort.order}`
    }
    if (pathParams !== '') {
      pathParams += '&';
    }
    pathParams += `limit=${this.taskPreferences.paginate.limit + 1}&skip=${this.taskPreferences.paginate.skip}`;

    return `?${pathParams}`;
  }

  constructor(private httpClient: HttpClient) {
  }

  getTasksFromServer(): Observable<Array<TaskModel>> {
    return this.httpClient.get<Array<TaskModel>>(`http://localhost:3000/tasks${this.getTaskPreferencesPathParams}`);
  }

  updateTask(taskId: string, update: { completed?: boolean, description?: string }): Observable<TaskModel> {
    if (Object.keys(update)) {
      return this.httpClient.patch<TaskModel>(`http://localhost:3000/tasks/${taskId}`, update);
    }
  }

  createTask(description: string, completed: boolean): Observable<TaskModel> {
    return this.httpClient.post<TaskModel>('http://localhost:3000/tasks', {description, completed});
  }

  deleteTask(id: string) {
    return this.httpClient.delete(`http://localhost:3000/tasks/${id}`);
  }
}

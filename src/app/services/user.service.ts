import { Injectable } from '@angular/core';

import { UserModel } from "../utilities/models/user.models";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private avatarBinary: string | ArrayBuffer = null;

  constructor(private httpClient: HttpClient) {
  }

  getUser(): Observable<UserModel> {
    return this.httpClient.get<UserModel>('http://localhost:3000/users/me');
  }

  updateUser(updates): Observable<UserModel> {
    if (Object.keys(updates).length)
      return this.httpClient.patch<UserModel>('http://localhost:3000/users/me', updates);
  }

  logoutUser(): Observable<object> {
    return this.httpClient.post<object>('http://localhost:3000/users/logout', {});
  }

  logoutUserOnAllDevices(): Observable<object> {
    return this.httpClient.post<object>('http://localhost:3000/users/logoutAll', {});
  }

  deleteUser(): Observable<UserModel> {
    return this.httpClient.delete<UserModel>('http://localhost:3000/users/me');
  }

  uploadAvatar(file): Observable<object> {
    return this.httpClient.post<object>('http://localhost:3000/users/me/avatar', file, {
      observe: 'events',
      reportProgress: true
    });
  }

  getAvatar(userId: string): Observable<Blob> {
    return this.httpClient.get(`http://localhost:3000/users/${userId}/avatar`, {responseType: 'blob'});
  }

  saveAvatarBinary(avatarBinary: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.avatarBinary = reader.result;
    }, false);

    if (avatarBinary) {
      reader.readAsDataURL(avatarBinary);
    }
  }

  get getAvatarBinary(): string | ArrayBuffer {
    return this.avatarBinary;
  }
}

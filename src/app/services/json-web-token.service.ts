import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { UserLoginResponseModel } from "../utilities/models/user.models";

@Injectable({
  providedIn: 'root'
})
export class JsonWebTokenService {

  constructor(private httpClient: HttpClient) {
  }

  login(email: string, password: string): Observable<UserLoginResponseModel> {
    return this.httpClient.post<UserLoginResponseModel>('http://localhost:3000/users/login', {email, password});
  }

  register(email: string, password: string, name: string): Observable<UserLoginResponseModel> {
    return this.httpClient.post<UserLoginResponseModel>('http://localhost:3000/users', {email, password, name});
  }

  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  getToken(): string {
    return localStorage.getItem('token');
  }
}

import { Injectable } from '@angular/core';
import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class InformationService {
  private spinnerEnabled = false;
  private messages: Array<string> = [];
  public mesageSent: Subject<boolean> = new Subject();

  constructor() {
  }

  get isProgressSpinnerOn(): boolean {
    return this.spinnerEnabled;
  }

  switchSpinner(): void {
    this.spinnerEnabled = !this.spinnerEnabled;
  }

  sendMessage(message: string) {
    this.messages.push(message);
    this.mesageSent.next(false);
    setTimeout(() => {
      this.messages.splice(0, 1);
      if (!this.messages.length) {
        this.mesageSent.next(true);
      }
    }, 3000);
  }

  get getMessages(): Array<string> {
    return this.messages;
  }

}

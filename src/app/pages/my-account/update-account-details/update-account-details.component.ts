import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserChanges, UserLoginResponseModel, UserModel } from "../../../utilities/models/user.models";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
  getFirstErrorKeyOfFormControl, MyErrorStateMatcher,
  passwordConfirmationValidator,
  validationErrorKeyToMessage
} from "../../../utilities/validation/valiation.utilities";

@Component({
  selector: 'app-update-account-details',
  templateUrl: './update-account-details.component.html',
  styleUrls: ['./update-account-details.component.scss']
})
export class UpdateAccountDetailsComponent implements OnInit {
  @Input('user') user: UserModel;
  @Output() userChanges: EventEmitter<UserChanges> = new EventEmitter();
  updatesForm: FormGroup;
  myMatcher = new MyErrorStateMatcher();

  constructor() {
  }

  ngOnInit() {
    this.updatesForm = new FormGroup({
      'name': new FormControl('', [
        Validators.maxLength(50)
      ]),
      'password': new FormControl('', [
        Validators.minLength(6),
        Validators.maxLength(50)
      ]),
      'confirm-password': new FormControl('')
    }, passwordConfirmationValidator);
  }

  getErrorMessage(formControlName: string): string {
    return validationErrorKeyToMessage(getFirstErrorKeyOfFormControl((<FormControl>this.updatesForm.get(formControlName))));
  }

  onUpdateFormSubmit() {
    this.updatesForm.get('confirm-password').markAsTouched();
    this.updatesForm.get('confirm-password').markAsDirty();

    if (this.updatesForm.valid) {
      let changes = {};
      if (!!this.updatesForm.controls.password.value) {
        changes['password'] = this.updatesForm.controls.password.value
      }
      if (!!this.updatesForm.controls.name.value && this.updatesForm.controls.name.value !== this.user.name) {
        changes['name'] = this.updatesForm.controls.name.value
      }
      this.userChanges.emit(changes)
    }
  }

}

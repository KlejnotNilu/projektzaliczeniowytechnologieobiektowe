import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/user.service";
import { Router } from "@angular/router";
import { JsonWebTokenService } from "../../services/json-web-token.service";
import { UserChanges, UserModel } from "../../utilities/models/user.models";
import { InformationService } from "../../services/information.service";

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {
  user: UserModel = null;
  avatarFile;

  constructor(private userService: UserService,
              private jwtService: JsonWebTokenService,
              private router: Router,
              private infoService: InformationService) {
  }

  ngOnInit() {
    if (!this.jwtService.getToken()) {
      this.router.navigate(['login']);
    }
    this.infoService.switchSpinner();
    this.userService.getUser().subscribe({
      next: (user: UserModel) => {
        this.user = user;
        if (this.userService.getAvatarBinary === null) {
          this.userService.getAvatar(this.user._id).subscribe({
            next: (avatarBinary: Blob) => {
              this.userService.saveAvatarBinary(avatarBinary);
            },
            error: (err) => {
              this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');

            }
          });
        }
        this.infoService.switchSpinner();
      },
      error: (err) => {
        this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');

        this.infoService.switchSpinner();
        this.router.navigate(['login']);
      }
    });
  }

  onUserChanges(userChanges: UserChanges): void {
    this.infoService.switchSpinner();
    this.userService.updateUser(userChanges).subscribe({
      next: (user: UserModel) => {

        this.user = user;
        this.infoService.switchSpinner();
      },
      error: (err) => {
        this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');

        this.infoService.switchSpinner();
      }
    });
  }

  onLogoutAll(): void {
    this.userService.logoutUserOnAllDevices().subscribe({
      next: () => {
        this.router.navigate(['login']);
      },
      error: (err) => {
        this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');

      }
    });
  }

  onDeleteAccount(): void {
    this.userService.deleteUser().subscribe();
    this.router.navigate(['login']);
  }

  onFileChange(fileChangedEvent): void {
    this.avatarFile = fileChangedEvent.target.files[0];
  }

  onUploadAvatar(): void {
    this.infoService.switchSpinner();
    let avatarFormData = new FormData();
    avatarFormData.append('avatar', this.avatarFile, this.avatarFile.name);
    this.userService.uploadAvatar(avatarFormData).subscribe(() => {
      this.userService.getAvatar(this.user._id).subscribe({
        next: (avatarBinary: Blob) => {
          this.userService.saveAvatarBinary(avatarBinary);
          this.infoService.switchSpinner();

        },
        error: (err) => {
          this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');

          this.infoService.switchSpinner();

        }
      })
    });
  }
}

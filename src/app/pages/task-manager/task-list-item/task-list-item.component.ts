import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { TaskModel } from "../../../utilities/models/task.models";
import { TasksService } from "../../../services/tasks.service";

@Component({
  selector: 'app-task-list-item',
  templateUrl: './task-list-item.component.html',
  styleUrls: ['./task-list-item.component.scss']
})
export class TaskListItemComponent implements OnInit {
  @Input('task') task: TaskModel;
  @Output() deleted: EventEmitter<string> = new EventEmitter();
  inEditionMode = false;

  constructor(private taskService: TasksService) {
  }

  ngOnInit() {
  }

  switchEditionMode(newDescription = '') {
    if (this.inEditionMode) {
      this.taskService.updateTask(this.task._id,
        {description: newDescription})
        .subscribe({
          next: (response) => {

            this.task.description = response.description;
            this.task.updatedAt = response.updatedAt
          },
          error(err) {

          }
        });

    }
    this.inEditionMode = !this.inEditionMode
  }

  onDeleteTask() {
    this.deleted.emit(this.task._id);
  }
}

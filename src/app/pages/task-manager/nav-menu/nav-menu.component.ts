import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from "../../../services/user.service";
import { InformationService } from "../../../services/information.service";

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit {

  constructor(public router: Router,
              private userService: UserService,
              private infoService: InformationService) {
  }

  ngOnInit() {
  }

  onNavButtonClick(destination: string) {
    this.router.navigate(destination.split('/'));
  }

  onLogoutClick() {
    this.userService.logoutUser().subscribe({
      next: () => {
        localStorage.removeItem('token');
        this.router.navigate(['login']);
      },
      error: (err) => {
        this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');

      }
    });
  }

}

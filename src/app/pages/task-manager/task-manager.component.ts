import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";

import { TasksService } from "../../services/tasks.service";
import { TaskModel, TaskPreferencesModel } from "../../utilities/models/task.models";
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
  getFirstErrorKeyOfFormControl,
  validationErrorKeyToMessage
} from "../../utilities/validation/valiation.utilities";
import { CdkDragDrop, moveItemInArray, transferArrayItem } from "@angular/cdk/drag-drop";
import { InformationService } from "../../services/information.service";
import { MatBottomSheet } from "@angular/material";

@Component({
  selector: 'app-task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.scss']
})
export class TaskManagerComponent implements OnInit {
  $tasks: Observable<Array<TaskModel>>;
  completedTasks: Array<TaskModel> = [];
  incompletedTasks: Array<TaskModel> = [];
  createTaskForm: FormGroup;
  taskPreferences: TaskPreferencesModel;
  nextPageDoesNotExist = false;

  constructor(private taskService: TasksService,
              private router: Router,
              private infoService: InformationService) {
  }

  ngOnInit() {
    this.taskPreferences = this.taskService.getPreferences();

    this.reloadTasks();

    this.createTaskForm = new FormGroup({
      'description': new FormControl('', [Validators.required, Validators.maxLength(256)]),
      'completed': new FormControl(null, Validators.required)
    });
  }

  drop(event: CdkDragDrop<Array<TaskModel>>): void {
    if (!(event.previousContainer === event.container)) {
      this.infoService.switchSpinner();
      this.taskService.updateTask(event.previousContainer.data[event.previousIndex]._id,
        {completed: !event.previousContainer.data[event.previousIndex].completed})
        .subscribe({
          next: (response) => {

            event.previousContainer.data[event.previousIndex].completed = response.completed;
            event.previousContainer.data[event.previousIndex].updatedAt = response.updatedAt;
            if (event.previousContainer === event.container) {
              moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            } else {
              transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
            }
            this.infoService.switchSpinner();
          },
          error: (err) => {

            this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');
            this.infoService.switchSpinner();
          }
        });
    } else {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      } else {
        transferArrayItem(event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
      }
    }
  }

  getErrorMessage(formControlName: string): string {
    return validationErrorKeyToMessage(getFirstErrorKeyOfFormControl((<FormControl>this.createTaskForm.get(formControlName))));
  }

  reloadTasks(pageChanging = false) {
    this.infoService.switchSpinner();
    this.$tasks = this.taskService.getTasksFromServer();
    this.$tasks.subscribe({
      next: (response: Array<TaskModel>) => {
        if (response.length === this.taskPreferences.paginate.limit + 1) {
          response = response.slice(0, response.length - 1);
          this.nextPageDoesNotExist = false;
        } else {
          this.nextPageDoesNotExist = true;
        }
        this.completedTasks = [];
        this.incompletedTasks = [];
        response.forEach((task) => {
          if (task.completed) {
            this.completedTasks.push(task);
          } else {
            this.incompletedTasks.push(task);
          }
        });

        this.infoService.switchSpinner();
      },
      error: (err) => {
        this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');
        this.infoService.switchSpinner();
        if (err.status === 401) {
          this.router.navigate(['login']);
        }

      }
    });
  }

  pageChange(numberOfNewElements: number) {
    this.taskService.changePreferences({
      paginate: {
        limit: this.taskPreferences.paginate.limit,
        skip: this.taskPreferences.paginate.skip + numberOfNewElements
      }
    });
    this.taskPreferences = this.taskService.getPreferences();
    this.reloadTasks(true);
  }

  onCreateTaskSubmit(): void {
    if (this.createTaskForm.valid) {
      this.taskService.createTask(this.createTaskForm.controls.description.value, this.createTaskForm.controls.completed.value).subscribe({
        next: (response: TaskModel) => {
          if (response.completed) {
            this.completedTasks.push(response);
          } else {
            this.incompletedTasks.push(response);
          }
          this.createTaskForm.reset();
        },
        error: (err) => {
          this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');

        }
      });
    }
  }

  uponTaskDeletion(id: string): void {
    this.infoService.switchSpinner();
    this.taskService.deleteTask(id).subscribe({
      next: (response: TaskModel) => {
        if (response.completed) {
          this.completedTasks.forEach((task, index) => {
            if (task._id === response._id) {
              this.completedTasks.splice(index, 1);
            }
          });
        } else {
          this.incompletedTasks.forEach((task, index) => {
            if (task._id === response._id) {
              this.incompletedTasks.splice(index, 1);
            }
          });
        }
        this.infoService.switchSpinner();
      },
      error: (err) => {
        this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');

        this.infoService.switchSpinner();
      }
    })
  }
}

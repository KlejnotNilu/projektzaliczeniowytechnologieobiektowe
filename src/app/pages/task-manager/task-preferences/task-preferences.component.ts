import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { TasksService } from "../../../services/tasks.service";
import {
  getFirstErrorKeyOfFormControl,
  validationErrorKeyToMessage
} from "../../../utilities/validation/valiation.utilities";
import { ColumnNames, TaskPreferenceChangesModel, TaskPreferencesModel } from "../../../utilities/models/task.models";

@Component({
  selector: 'app-task-preferences',
  templateUrl: './task-preferences.component.html',
  styleUrls: ['./task-preferences.component.scss']
})
export class TaskPreferencesComponent implements OnInit {
  @Output() preferencesHasBeenUpdated: EventEmitter<boolean> = new EventEmitter();
  updatePreferencesForm: FormGroup;
  currentPreferences: TaskPreferencesModel;

  constructor(private tasksService: TasksService) {
  }

  ngOnInit() {
    this.currentPreferences = this.tasksService.getPreferences();
    this.updatePreferencesForm = new FormGroup({
      'completed': new FormControl('undefined'),
      'column-name': new FormControl('undefined'),
      'order': new FormControl('asc'),
      'limit': new FormControl('', [
        Validators.pattern(new RegExp('^\\d{1,2}$'))
      ])
    });
  }

  onUpdateSubmit(): void {
    let updates: TaskPreferenceChangesModel = {};
    if (this.updatePreferencesForm.controls['completed'].value !== String(this.currentPreferences.completed)) {
      updates['completed'] = this.updatePreferencesForm.controls.completed.value === 'undefined' ?
        undefined : this.updatePreferencesForm.controls.completed.value;
    }
    if (this.updatePreferencesForm.controls['column-name'].value !== String(this.currentPreferences.sort.columnName)) {
      updates['sort'] = {};
      updates['sort']['columnName'] = this.updatePreferencesForm.controls['column-name'].value === 'undefined' ?
        undefined : this.updatePreferencesForm.controls.completed.value;
      updates['sort']['order'] = this.updatePreferencesForm.controls['order'].value;
    }
    if (this.updatePreferencesForm.controls['limit'].value !== '' &&
      this.updatePreferencesForm.controls['limit'].value !== this.currentPreferences.paginate.limit) {
      updates['paginate'] = {};
      updates['paginate']['limit'] = this.updatePreferencesForm.controls['limit'].value;
    }
    if (Object.keys(updates)) {
      this.tasksService.changePreferences(updates);
      Object.assign(this.currentPreferences, updates);
      this.preferencesHasBeenUpdated.emit(true);
    }
  }

  getErrorMessage(formControlName: string): string {
    return validationErrorKeyToMessage(getFirstErrorKeyOfFormControl((<FormControl>this.updatePreferencesForm.get(formControlName))));
  }

}

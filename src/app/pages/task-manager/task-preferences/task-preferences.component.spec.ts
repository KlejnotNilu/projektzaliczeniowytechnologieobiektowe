import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskPreferencesComponent } from './task-preferences.component';

describe('TaskPreferencesComponent', () => {
  let component: TaskPreferencesComponent;
  let fixture: ComponentFixture<TaskPreferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaskPreferencesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskPreferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

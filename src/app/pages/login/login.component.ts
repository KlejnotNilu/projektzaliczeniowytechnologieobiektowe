import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import {
  getFirstErrorKeyOfFormControl,
  validationErrorKeyToMessage
} from "../../utilities/validation/valiation.utilities";
import { JsonWebTokenService } from "../../services/json-web-token.service";
import { UserLoginResponseModel } from "../../utilities/models/user.models";
import { HttpErrorResponse } from "@angular/common/http";
import { InformationService } from "../../services/information.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginError: HttpErrorResponse = null;

  constructor(private router: Router,
              private infoService: InformationService,
              private jwtService: JsonWebTokenService) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'email': new FormControl('', [
        Validators.required,
        Validators.maxLength(50),
        Validators.email
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(50)
      ])
    });
  }

  getErrorMessage(formControlName: string): string {
    return validationErrorKeyToMessage(getFirstErrorKeyOfFormControl((<FormControl>this.loginForm.get(formControlName))));
  }

  onLoginFormSubmit(): void {
    if (this.loginForm.valid) {
      this.jwtService.login(this.loginForm.get('email').value, this.loginForm.get('password').value).subscribe(
        {
          next: (response: UserLoginResponseModel) => {
            localStorage.setItem('token', response.token);
            this.router.navigate(['/']);
          },
          error: (err) => {
            this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');
            this.loginError = err;
          }
        }
      );
    }
  }

  onResetButtonClick(status: number): void {
    if (status === 400) {
      this.loginForm.reset();
      this.loginError = null;
    }
  }

  onSingUpClick(): void {
    this.router.navigate(['sign-up']);
  }

}

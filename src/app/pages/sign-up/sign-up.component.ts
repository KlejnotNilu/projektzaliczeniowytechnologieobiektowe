import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
  getFirstErrorKeyOfFormControl,
  validationErrorKeyToMessage,
  passwordConfirmationValidator,
  MyErrorStateMatcher
} from "../../utilities/validation/valiation.utilities";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { JsonWebTokenService } from "../../services/json-web-token.service";
import { UserLoginResponseModel } from "../../utilities/models/user.models";
import { InformationService } from "../../services/information.service";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  signUpError: HttpErrorResponse = null;
  matcher = new MyErrorStateMatcher();

  constructor(private router: Router,
              private infoService: InformationService,
              private jwtService: JsonWebTokenService) {
  }

  ngOnInit() {
    this.signUpForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      'email': new FormControl('', [
        Validators.required,
        Validators.maxLength(50),
        Validators.email
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(50)
      ]),
      'confirm-password': new FormControl('', Validators.required)
    }, passwordConfirmationValidator);
  }


  getErrorMessage(formControlName: string): string {
    return validationErrorKeyToMessage(getFirstErrorKeyOfFormControl((<FormControl>this.signUpForm.get(formControlName))));
  }

  onSignUpFormSubmit() {
    this.signUpForm.get('confirm-password').markAsTouched();
    this.signUpForm.get('confirm-password').markAsDirty();

    if (this.signUpForm.valid) {
      this.jwtService.register(this.signUpForm.get('email').value,
        this.signUpForm.get('password').value,
        this.signUpForm.get('name').value)
        .subscribe(
          {
            next: (response: UserLoginResponseModel) => {
              this.jwtService.setToken(response.token);
              this.router.navigate(['/']);
            },
            error: (err) => {
              this.infoService.sendMessage('Sorry, we had some troubles. Please try again in a minute!');
              this.signUpError = err;
            }
          }
        );
    }
  }

  onResetButtonClick(status: number) {
    if (status === 400) {
      this.signUpForm.reset();
    }
  }

  onLogInClick() {
    this.router.navigate(['login']);
  }
}

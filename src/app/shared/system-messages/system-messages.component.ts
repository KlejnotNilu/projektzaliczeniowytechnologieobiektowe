import { Component, OnInit } from '@angular/core';
import { InformationService } from "../../services/information.service";
import { MatBottomSheet } from "@angular/material";

@Component({
  selector: 'app-system-messages',
  templateUrl: './system-messages.component.html',
  styleUrls: ['./system-messages.component.scss']
})
export class SystemMessagesComponent implements OnInit {

  constructor(private infoService: InformationService,
              private matBottomSheet: MatBottomSheet) {
  }

  ngOnInit(): void {
    this.infoService.mesageSent.subscribe((event) => {
      if (event) {
        this.matBottomSheet.dismiss();
      }
    })
  }

}

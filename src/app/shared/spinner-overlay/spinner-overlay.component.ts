import { Component, OnInit } from '@angular/core';
import { InformationService } from "../../services/information.service";

@Component({
  selector: 'app-spinner-overlay',
  templateUrl: './spinner-overlay.component.html',
  styleUrls: ['./spinner-overlay.component.scss']
})
export class SpinnerOverlayComponent implements OnInit {

  constructor(private infoService: InformationService) {
  }

  ngOnInit() {
  }

}

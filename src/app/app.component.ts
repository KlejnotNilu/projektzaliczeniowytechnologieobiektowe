import { Component, OnInit } from '@angular/core';
import { InformationService } from "./services/information.service";
import { MatBottomSheet } from "@angular/material";
import { SystemMessagesComponent } from "./shared/system-messages/system-messages.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private infoService: InformationService,
              private matBottomSheet: MatBottomSheet) {
  }

  ngOnInit(): void {
    this.infoService.mesageSent.subscribe(() => {
      this.matBottomSheet.open(SystemMessagesComponent);
    });
  }

}

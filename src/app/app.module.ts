import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { TaskManagerComponent } from './pages/task-manager/task-manager.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatBottomSheetModule,
  MatButtonModule,
  MatCardModule, MatChipsModule,
  MatDividerModule, MatExpansionModule,
  MatFormFieldModule,
  MatInputModule, MatProgressSpinnerModule, MatSelectModule
} from "@angular/material";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { AuthInterceptor } from "./interceptors/auth.interceptor";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { TaskListItemComponent } from './pages/task-manager/task-list-item/task-list-item.component';
import { NavMenuComponent } from './pages/task-manager/nav-menu/nav-menu.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { UpdateAccountDetailsComponent } from './pages/my-account/update-account-details/update-account-details.component';
import { AvatarComponent } from './shared/avatar/avatar.component';
import { TaskPreferencesComponent } from './pages/task-manager/task-preferences/task-preferences.component';
import { SpinnerOverlayComponent } from './shared/spinner-overlay/spinner-overlay.component';
import { SystemMessagesComponent } from './shared/system-messages/system-messages.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    TaskManagerComponent,
    TaskListItemComponent,
    NavMenuComponent,
    MyAccountComponent,
    UpdateAccountDetailsComponent,
    AvatarComponent,
    TaskPreferencesComponent,
    SpinnerOverlayComponent,
    SystemMessagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatFormFieldModule,
    DragDropModule,
    MatExpansionModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatBottomSheetModule,
    MatChipsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    SystemMessagesComponent
  ]
})
export class AppModule {
}

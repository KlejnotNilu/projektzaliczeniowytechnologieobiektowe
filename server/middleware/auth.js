/*jshint esversion: 6 */
const jwt = require('jsonwebtoken');

const UserModel = require('../models/user-model');

const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const decoded = jwt.verify(token, 'bardzoSlabySekret');
        const user = await UserModel.findOne({ _id: decoded.id, 'tokens.token': token });

        if(user) {
            req.user = user;
            req.token = token;
            next();
        } else {
            throw new Error();
        }
    } catch (e) {
        res.status(401).send({error: 'Please authenticate'});
    }
};

module.exports = auth;

/*jshint esversion: 6 */

const express = require('express');

const auth = require('../middleware/auth');
const TaskModel = require('../models/task-model');

const router = new express.Router();

router.post('/tasks', auth, async (req, res) => {
    const task = new TaskModel({
        ...req.body,
        owner: req.user._id
    });

    try {
        await task.save();
        res.status(201).send(task);
    } catch (e) {
        res.status(400).send();
    }
});

router.get('/tasks', auth, async (req, res) => {
    const match = {};
    const sort = {};

    if(req.query.sort && req.query.sort.split(':').length === 2){
        sort[req.query.sort.split(':')[0]] = req.query.sort.split(':')[1] === 'asc' ? 1 : -1
    }

    if(req.query.completed) {
        match.completed = req.query.completed.toLowerCase() === 'true'
    }

    try {
        await req.user.populate({
            path : 'tasks',
            match,
            options: {
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.skip),
                sort
            }
        }).execPopulate();
        res.send(req.user.tasks);
    } catch (e) {
        res.status(500).send()
    }
});

router.get('/tasks/:id', auth, async (req, res) => {
    const _id = req.params.id;

    try {
        const task = await TaskModel.findOne({_id, owner: req.user._id});

        if (task) {
            res.send(task);
        } else {
            res.status(404).send();
        }
    } catch (e) {
        res.send(500).send();
    }
});

router.patch('/tasks/:id', auth, async (req, res) => {
    const allowedUpdates = [
        'description',
        'completed'
    ];
    let operationAllowed = true;
    Object.keys(req.body).forEach((update) => {
        if (!allowedUpdates.includes(update)) {
            operationAllowed = false;
        }
    });
    if (!operationAllowed) {
        return res.status(400).send({error: 'Invalida updates!'});
    }
    try {
        const task = await TaskModel.findOne({_id: req.params.id, owner: req.user._id});
        if (task) {
            Object.keys(req.body).forEach((change) => {
                task[change] = req.body[change];
            });

            await task.save();

            res.send(task);
        } else {
            res.status(404).send();
        }
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/tasks/:id', auth, async (req, res) => {
    try {
        const task = await TaskModel.findOneAndDelete({_id: req.params.id, owner: req.user._id});
        if (task) {
            res.send(task);
        } else {
            res.status(404).send();
        }
    } catch (e) {
        res.status(500).send();
    }
});

module.exports = router;

/*jshint esversion: 6 */

const express = require('express');
const multer = require('multer');
const sharp = require('sharp');

const auth = require('../middleware/auth');
const UserModel = require('../models/user-model');

const router = new express.Router();
const avatarUpload = multer({
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, callback) {
        if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(file.originalname)) {
            return callback(new Error('Please upload an image!'));
        }

        callback(undefined, true);
    }
});

router.get('/users/me', auth, async (req, res) => {
    res.send(req.user);
});

router.post('/users/login', async (req, res) => {
    try {
        const user = await UserModel.findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({user, token});
    } catch (e) {
        res.status(400).send();
    }
});

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token;
        });
        await req.user.save();

        res.send();
    } catch (e) {
        res.stats(500).send();
    }
});

router.post('/users/logoutAll', auth, async (req, res) => {
    try {
        req.user.tokens = [];
        await req.user.save();

        res.send();
    } catch (e) {
        res.status(500).send()
    }
});

router.patch('/users/me', auth, async (req, res) => {
    const allowedUpdates = [
        'name',
        'email',
        'password',
        'age'
    ];
    let operationAllowed = true;
    Object.keys(req.body).forEach((update) => {
        if (!allowedUpdates.includes(update)) {
            operationAllowed = false;
        }
    });
    if (!operationAllowed) {
        return res.status(400).send({error: 'Invalid updates!'});
    }

    const user = req.user;

    try {
        Object.keys(req.body).forEach((updateAttr) => {
            user[updateAttr] = req.body[updateAttr];
        });

        await user.save();
        res.send(user);

    } catch (e) {
        res.status(400).send(e);
    }
});

router.post('/users', async (req, res) => {
    const user = new UserModel(req.body);
    try {
        await user.save();
        const token = await user.generateAuthToken();
        res.status(201).send({user, token});
    } catch (e) {
        console.log(e);
        res.status(400).send();
    }
});

router.delete('/users/me', auth, async (req, res) => {
    try {
        await req.user.remove();
        res.status(404).send(req.user);
    } catch (e) {
        res.status(500).send();
    }
});

router.post('/users/me/avatar', auth, avatarUpload.single('avatar'), async (req, res) => {
  req.user.avatar = await sharp(req.file.buffer).resize({width: 250, height: 250}).png().toBuffer();
    await req.user.save();
    res.send();
}, (error, req, res, next) => {
    res.status(400).send({error: error.message});
});

router.get('/users/:id/avatar', async (req, res) => {
    try {
        const user = await UserModel.findById(req.params.id);
        if( !user || !user.avatar) {
            throw new Error('No avatar found');
        }

        res.set('Content-Type', 'image/png');
        res.send(user.avatar);
    } catch (e) {
        res.status(404).send()
    }
});

router.delete('/users/me/avatar', auth, async (req, res) => {
   req.user.avatar = undefined;
   await req.user.save();
   res.send()
});

module.exports = router;

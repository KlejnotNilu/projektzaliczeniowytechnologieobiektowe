/*jshint esversion: 6 */

const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const TaskModel = require('../models/task-model');

const userSchema = new mongoose.Schema({
        name: {
            type: String,
            required: true,
            trim: true
        },
        email: {
            type: String,
            required: true,
            unique: true,
            trim: true,
            lowercase: true
        },
        age: {
            type: Number,
            validate(value) {
                if (value < 0) {
                    throw new Error('Age must be a positive number!');
                }
            }
        },
        password: {
            type: String,
            required: true,
            trim: true,
            minlength: 6,
            validate(value) {
                if (value.toLowerCase().indexOf('password') !== -1) {
                    throw new Error('Password is way to stupid');
                }
            }
        },
        tokens: [{
            token: {
                type: String,
                required: true
            }
        }],
        avatar: {
            type: Buffer
        }
    },
    {
        timestamps: true
    });

userSchema.virtual('tasks', {
    ref: 'TaskModel',
    localField: '_id',
    foreignField: 'owner'
});

userSchema.statics.findByCredentials = async (email, password) => {
    const user = await UserModel.findOne({email});

    if (!user) {
        throw new Error('Unable to login');
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
        throw new Error('Unable to login');
    }

    return user;
};

userSchema.methods.generateAuthToken = async function () {
    const token = await jwt.sign({id: this._id.toString()}, 'bardzoSlabySekret', {expiresIn: '12 hours'});

    this.tokens = this.tokens.concat({token});

    await this.save();

    return token;
};

userSchema.methods.toJSON = function () {
    const userObject = this.toObject();

    delete userObject.password;
    delete userObject.tokens;
    delete userObject.avatar;

    return userObject;
};

// Middleware: has password before saving
userSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 8);
    }

    next();
});

// Middleware: remove tasks upon deleting user
userSchema.pre('remove', async function (next) {
    await TaskModel.deleteMany({owner: this._id});

    next()
});

const UserModel = mongoose.model('UserModel', userSchema);

module.exports = UserModel;

/*jshint esversion: 6 */

const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
        description: {
            type: String,
            required: true,
            trim: true
        },
        completed: {
            type: Boolean,
            default: false
        },
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            require: true,
            ref: 'UserModel'
        }
    },
    {
        timestamps: true
    });

const TaskModel = mongoose.model('TaskModel', taskSchema);

module.exports = TaskModel;

/*jshint esversion: 6 */

const express = require('express');

require('./db/mongoose');
const userRouter = require('./routers/user-router');
const taskRouter = require('./routers/task-router');

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(function(req, res, next) {

  res.setHeader('Access-Control-Allow-Origin', "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'authorization, content-type');
  next();
});
app.use(userRouter);
app.use(taskRouter);

app.listen(port, () => {
    console.log(`Server listening on port: ${port}`);
});
